﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstados.Models;
using ApiEstados.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiEstados.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadosController : ControllerBase
    {
        private readonly IRepository<ESTADO> IRepository;
        public EstadosController(IRepository<ESTADO> IRep)
        {
            this.IRepository = IRep;
        }

        // GET: api/<EstadosController>
        [HttpGet]
        public IEnumerable<ESTADO> Get()
        {
           return this.IRepository.ListarTodos();
        }

        // GET api/<EstadosController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
              
    }
}
