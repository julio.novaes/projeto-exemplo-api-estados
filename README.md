# Projeto Exemplo Deploy Heroku

Projeto de exemplo com dotnet core 3.1 usando ORM e banco de dados PostgreSQL do próprio heroku; Tudo faz parte do pacote gratuito do heroku.
Projeto consiste em uma api que retorna em formato JSON um lista dos estados do brasil com sua sigla nome e código do ibge.

## Acesso ao app

* [api-estados](https://apiestados.herokuapp.com/api/estados)

## Observação

Como o heroku não possui suporte direto para C#, então é necessário usar o deploy via container usado docker

## Deploy no  heroku

### Pré-requisitos 

* [Docker](https://www.docker.com/products/docker-desktop)
* [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
 
### Login no heroku 

`heroku login`

`heroku container:login`

### Build imagem da aplicação
 
* Dentro da pasta da aplicação execute o comando de build
 
`docker build --rm -f "Dockerfile" -t "apiestados:latest" .`
 
 > não esqueça do ponto no fim do comando para identificar o diretório corrente 
 
### Fazer push do container

`heroku container:push web -a apiestados`

### Para finalizar 

`heroku container:release web -a apiestados ` 

### Logs

`heroku logs — tail -a apiestados`
