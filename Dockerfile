FROM julionovaessouza/dot-net-mega-app:2.0

ADD ./ApiEstados.zip  /home/

RUN unzip /home/ApiEstados.zip -d /home/

RUN chmod +x /home/app/build/exec.sh

EXPOSE 80

RUN DOTNET_RUNNING_IN_CONTAINER=true

CMD [ "/home/app/build/exec.sh" ]
