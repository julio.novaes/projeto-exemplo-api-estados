﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstados.Models.DbContexts
{
    public class EstadosDbContext : DbContext
    {

        public DbSet<ESTADO> estados_tb { get; set; }


        public EstadosDbContext(DbContextOptions<EstadosDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ESTADO>()
            .HasKey(o => o.codigo_ibge);

        }
    }
}

