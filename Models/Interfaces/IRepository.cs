﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApiEstados.Models.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> ListarTodos();
        IQueryable<T> Query();
        IQueryable<T> Query(Expression<Func<T, bool>> predicate);
        void DetachLocal(Expression<Func<T, bool>> predicate);

    }
}
