﻿using ApiEstados.Models.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApiEstados.Models.Interfaces
{
    public class EstadosRepository : IRepository<ESTADO>
    {

        private readonly EstadosDbContext ctx;

        public EstadosRepository(EstadosDbContext ctx)
        {
            this.ctx = ctx;
        }

        public void DetachLocal(Expression<Func<ESTADO, bool>> predicate)
        {
            var local = ctx.estados_tb.Where(predicate).FirstOrDefault();
            if (local != null)
            {
                ctx.Entry(local).State = EntityState.Detached;
            }
        }



        public IEnumerable<ESTADO> ListarTodos()
        {
            return ctx.estados_tb.ToList();
        }

        public IQueryable<ESTADO> Query()
        {
            return ctx.estados_tb;
        }

        public IQueryable<ESTADO> Query(Expression<Func<ESTADO, bool>> predicate)
        {
            return ctx.estados_tb.Where(predicate);
        }
    }
    
}

