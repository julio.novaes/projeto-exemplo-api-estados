﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstados.Models
{
    public class ESTADO
    {
        public int codigo_ibge { get; set; }
        public string sigla { get; set; }
        public string nome { get; set; }
    }

}
