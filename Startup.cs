using ApiEstados.Models.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using Microsoft.EntityFrameworkCore;
using ApiEstados.Models.Interfaces;
using ApiEstados.Models;
using System;

namespace ApiEstados
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            DotNetEnv.Env.Load();
            
            string connection = @"Server="+ DotNetEnv.Env.GetString("DB_SERVICE")+";" +
                                "user id ="+DotNetEnv.Env.GetString("DB_USER_ID")+";" +
                                "password ="+DotNetEnv.Env.GetString("DB_PASSWORD")+";" +
                                "database="+DotNetEnv.Env.GetString("DB_NAME")+";" +
                                "SSL Mode="+DotNetEnv.Env.GetString("DB_SSLMODE")+";" +
                                "Trust Server Certificate="+DotNetEnv.Env.GetString("DB_TSC");
            
            services.AddDbContext<EstadosDbContext>(options =>
                                               options.UseNpgsql(connection)
                                              );
            services.AddTransient<IRepository<ESTADO>, EstadosRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
